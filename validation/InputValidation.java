package Vaildations;

import com.sun.org.apache.bcel.internal.generic.RETURN;

import ZubankDto.CreateAccountAdminDto;
import ZubankDto.CreateAccountCustomerDto;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class InputValidation {

	public static JSONObject customerAccountValidation(CreateAccountCustomerDto customerDto, int accountId) {

		JSONObject createCustomerAccountValidation = new JSONObject();

		createCustomerAccountValidation.put("userAccountId", accountId);

		if (customerDto.getCustomer_fname() != null && customerDto.getCustomer_fname().matches("[a-zA-Z]+")) {

			createCustomerAccountValidation.put("customerFirstNameStatus", "success");

		} else {

			createCustomerAccountValidation.put("customerFirstNameStatus", "failure");

		}

		if (customerDto.getCustomer_lname() != null && customerDto.getCustomer_lname().matches("[a-zA-Z]+")) {

			createCustomerAccountValidation.put("customerLastNameStatus", "success");

		} else {

			createCustomerAccountValidation.put("customerLastNameStatus", "failure");

		}

		if (customerDto.getAadhar_id() != null && customerDto.getAadhar_id().matches("[0-9]+")
				&& customerDto.getAadhar_id().length() == 12) {

			createCustomerAccountValidation.put("customerAadharStatus", "success");

		} else {

			createCustomerAccountValidation.put("customerAadharStatus", "failure");

		}

		if (customerDto.getGmail() != null
				&& customerDto.getGmail().matches("^[!#$%&'*\\/=?^_+-`{|}~a-zA-Z0-9]{6,30}[@](gmail)[.]com$")) {

			createCustomerAccountValidation.put("customerGmailStatus", "success");

		} else if (customerDto.getGmail() != null
				&& customerDto.getGmail().matches("^[!#$%&'*\\/=?^_+-`{|}~a-zA-Z0-9]{6,30}[@](zohocorp)[.]com$")) {

			createCustomerAccountValidation.put("customerGmailStatus", "success");

		} else {

			createCustomerAccountValidation.put("customerGmailStatus", "failure");

		}

		if (customerDto.getPhone_no() != null && customerDto.getPhone_no().matches("[0-9]+")
				&& customerDto.getPhone_no().length() == 10) {

			createCustomerAccountValidation.put("customerPhoneNoStatus", "success");

		} else {

			createCustomerAccountValidation.put("customerPhoneNoStatus", "failure");

		}

		return createCustomerAccountValidation;
	}

	public static JSONObject adminAccountValidation(CreateAccountAdminDto adminDto, int accountId) {

		JSONObject createAdminAccountValidation = new JSONObject();

		createAdminAccountValidation.put("userAccountId", accountId);

		if (adminDto.getAdmin_fname() != null && adminDto.getAdmin_fname().matches("[a-zA-Z]+")) {

			createAdminAccountValidation.put("adminFirstNameStatus", "success");

		} else {

			createAdminAccountValidation.put("adminFirstNameStatus", "failure");

		}

		if (adminDto.getAdmin_lname() != null && adminDto.getAdmin_lname().matches("[a-zA-Z]+")) {

			createAdminAccountValidation.put("adminLastNameStatus", "success");

		} else {

			createAdminAccountValidation.put("adminLastNameStatus", "failure");

		}

		if (adminDto.getAadhar_id() != null && adminDto.getAadhar_id().matches("[0-9]+")
				&& adminDto.getAadhar_id().length() == 12) {

			createAdminAccountValidation.put("adminAadharStatus", "success");

		} else {

			createAdminAccountValidation.put("adminAadharStatus", "failure");

		}

		if (adminDto.getGmail() != null
				&& adminDto.getGmail().matches("^[!#$%&'*\\/=?^_+-`{|}~a-zA-Z0-9]{6,30}[@](gmail)[.]com$")) {

			createAdminAccountValidation.put("customerGmailStatus", "success");

		} else if (adminDto.getGmail() != null
				&& adminDto.getGmail().matches("^[!#$%&'*\\/=?^_+-`{|}~a-zA-Z0-9]{6,30}[@](zohocorp)[.]com$")) {

			createAdminAccountValidation.put("customerGmailStatus", "success");

		} else {

			createAdminAccountValidation.put("customerGmailStatus", "failure");

		}

		if (adminDto.getPhone_no() != null && adminDto.getPhone_no().matches("[0-9]+")
				&& adminDto.getPhone_no().length() == 10) {

			createAdminAccountValidation.put("customerPhoneNoStatus", "success");

		} else {

			createAdminAccountValidation.put("customerPhoneNoStatus", "failure");

		}

		return createAdminAccountValidation;

	}

}
