package ZuBank;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import Vaildations.InputValidation;
import ZubankDto.CreateAccountCustomerDto;
import ZubankDto.SendMailDTO;
import ZubankUtility.ConstantValue;
import ZubankUtility.Schedularformail;
import ZubankUtility.UtilityZuBank;
import net.sf.json.JSONObject;



@MultipartConfig

public class CreateAccountCustomer  extends HttpServlet{
	

	private static final long serialVersionUID = 1L;
	public static ArrayList <SendMailDTO> mailSendingList = new ArrayList<SendMailDTO>();
	public long newImage = System.currentTimeMillis();
	
	public void init() {
		
		
		 Timer timer = new Timer();
	        TimerTask task = new Schedularformail();

	        Calendar threepm = Calendar.getInstance();
	        threepm.set(Calendar.HOUR_OF_DAY, 22);
	        threepm.set(Calendar.MINUTE, 55);
	        threepm.set(Calendar.SECOND, 0);

	        timer.schedule(task, threepm.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));

	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		
		
		CreateAccountCustomerDto customerDto = new CreateAccountCustomerDto();
		
		Part imageFile=req.getPart("profile_image");
		
		int accountId = (int) req.getSession().getAttribute("user");
		
		customerDto.setCustomer_fname(req.getParameter("customer_fname"));
	    customerDto.setCustomer_lname(req.getParameter("customer_lname"));
		customerDto.setAadhar_id(req.getParameter("aadhar_id"));
		customerDto.setGmail(req.getParameter("gmail"));
		customerDto.setPhone_no(req.getParameter("phone_no"));
		
		System.out.println(customerDto.getCustomer_fname());
		
		JSONObject inputValidationStatus = InputValidation.customerAccountValidation(customerDto, accountId);
	
		System.out.println(inputValidationStatus);
		System.out.println(inputValidationStatus.size());
		
		JSONObject validationCheckResult = validationCheck(inputValidationStatus);
		
		
		if(validationCheckResult.size()==0){
			uploadfile(customerDto,imageFile);
			createAccountForCustomer(customerDto);
			validationCheckResult.put("status", "success");
			res.setContentType("application/json");
     	    res.setCharacterEncoding("UTF-8");
        	res.getWriter().write(validationCheckResult.toString());
			System.out.println("data uploaded....");
		}else{
			res.setContentType("application/json");
     	    res.setCharacterEncoding("UTF-8");
        	res.getWriter().write(validationCheckResult.toString());
			System.out.println("data not uploaded...");
		}
			

		System.out.print("success.....");
	
        res.sendRedirect("AdminViewPage.jsp?status=success");
      }
	
	
	public void uploadfile(CreateAccountCustomerDto customerDto,Part file) throws IOException{
		   
			InputStream iosValue = null;
			BufferedImage biValue = null;
			
			iosValue = file.getInputStream();
			
			biValue = ImageIO.read(iosValue);
			
			File outputfile = new File(ConstantValue.INSERT_CUSTOMER_IMAGE_FILE_PATH + newImage + ".png");
			ImageIO.write(biValue, "png", outputfile);
			
	}
	
	public void createAccountForCustomer(CreateAccountCustomerDto customerDto){
		
		int maxCustomerId = 0;
		int accountNumber = 0;
		int mailSendingListSize = 0;
		String accountPassword = null;
		String pinNumber = null;
		String accountPasswordEncryption = null;
		String pinNumberEncryption = null;
		String imageFilePath = null;
		try{

			UtilityZuBank.connectionManager();
			
	        PreparedStatement statementFetchMaxCustomerId=UtilityZuBank.con.prepareStatement(ConstantValue.FECTH_MAX_CUSTOMER_ID);
	        ResultSet resultSetMaxCustomerId = statementFetchMaxCustomerId.executeQuery();

	        while (resultSetMaxCustomerId.next()) {

	            maxCustomerId = resultSetMaxCustomerId.getInt("maxstudvalue");

	        }
	        accountNumber = maxCustomerId+1;
		
			accountPassword = String.valueOf(System.currentTimeMillis()).substring(5,13);
			pinNumber = String.valueOf(System.currentTimeMillis()).substring(7,11);
			
			SendMailDTO mailDto = new SendMailDTO();
			mailDto.setAccountId(String.valueOf(accountNumber));
			mailDto.setPassword(accountPassword);
			mailDto.setCardId(customerDto.getAadhar_id());
			mailDto.setPinNumber(pinNumber);
			mailDto.setCustFname(customerDto.getCustomer_fname());
			mailDto.setGmail(customerDto.getGmail());
			
			mailSendingList.add(mailDto);
			mailSendingListSize = mailSendingList.size();
			
	        accountPasswordEncryption = UtilityZuBank.encryption(accountPassword);
	        pinNumberEncryption = UtilityZuBank.encryption(pinNumber);
			
            PreparedStatement statementInsertCustomerAccountDetails = UtilityZuBank.con.prepareStatement(ConstantValue.INSERT_CUSTOMER_ACCOUNT_DETAILS);
            statementInsertCustomerAccountDetails.setString(1, customerDto.getCustomer_fname());
            statementInsertCustomerAccountDetails.setString(2, customerDto.getCustomer_lname());
            statementInsertCustomerAccountDetails.setString(3, customerDto.getAadhar_id());
            statementInsertCustomerAccountDetails.setString(4, String.valueOf(accountNumber));
            statementInsertCustomerAccountDetails.setString(5, customerDto.getGmail());
            statementInsertCustomerAccountDetails.setString(6, customerDto.getPhone_no());
            statementInsertCustomerAccountDetails.setString(7, accountPasswordEncryption);
            statementInsertCustomerAccountDetails.setString(8, pinNumberEncryption);
            statementInsertCustomerAccountDetails.setInt(9, 100);            
           
            statementInsertCustomerAccountDetails.executeUpdate();
            
            imageFilePath = "./img/"+newImage+".png";
            
            PreparedStatement statementInsertCustomerImageDetails = UtilityZuBank.con.prepareStatement(ConstantValue.INSERT_CUSTOMER_IMAGE_DETAILS);
            statementInsertCustomerImageDetails.setInt(1, accountNumber);
            statementInsertCustomerImageDetails.setString(2, imageFilePath);
            
            statementInsertCustomerImageDetails.executeUpdate();
            
            System.out.print("data updated");
           
            
           
		UtilityZuBank.con.close();
    }catch(Exception e){
    	
    	
    	}
		
		
	}
	
	public JSONObject validationCheck(JSONObject inputValidationStatus){
		
		JSONObject json =new JSONObject();
		Iterator<String> keys1 = inputValidationStatus.keys();
		
		while( keys1.hasNext() ) {
		    String key1 = (String) keys1.next();
		    Object value1 = inputValidationStatus.get(key1);
		    System.out.println("Key: " + key1);
		    System.out.println("Value: " +value1 );
		    
		    if(value1.equals("failure")){

		    	json.put(key1, value1);
		    	
		    }
		   
		}
		return json;
		

	}

}
