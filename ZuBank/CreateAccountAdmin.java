package ZuBank;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import Vaildations.InputValidation;
import ZubankDto.CreateAccountAdminDto;
import ZubankDto.SendMailDTO;
import ZubankUtility.ConstantValue;
import ZubankUtility.UtilityZuBank;
import net.sf.json.JSONObject;

@MultipartConfig

public class CreateAccountAdmin extends HttpServlet{
	
	public long newimage = System.currentTimeMillis();

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		CreateAccountAdminDto adminDto = new CreateAccountAdminDto();

		int accountId = (int) req.getSession().getAttribute("user");

		Part file = req.getPart("profile_image");
		adminDto.setAdmin_fname(req.getParameter("admin_fname"));
		adminDto.setAdmin_lname(req.getParameter("admin_lname"));
		adminDto.setAadhar_id(req.getParameter("aadhar_id"));
		adminDto.setGmail(req.getParameter("gmail"));
		adminDto.setPhone_no(req.getParameter("phone_no"));

		JSONObject inputValidationStatus=InputValidation.adminAccountValidation(adminDto, accountId);

		JSONObject validationCheckResult = validationCheck(inputValidationStatus);
		
		if(validationCheckResult.size()==0){
			//createaccount(adminDto, file);
			validationCheckResult.put("status", "success");
			res.setContentType("application/json");
     	    res.setCharacterEncoding("UTF-8");
        	res.getWriter().write(validationCheckResult.toString());
			System.out.println("data uploaded....");
		}else{
			res.setContentType("application/json");
     	    res.setCharacterEncoding("UTF-8");
        	res.getWriter().write(validationCheckResult.toString());
			System.out.println("data not uploaded...");
		}
			
		
		res.sendRedirect("AdminViewPage.jsp");
	}

	public void createaccount(CreateAccountAdminDto adminDto, Part file) {

		int maxAdminId = 0;
		int accountNumber = 0;
		String accountPassword = null;
		String accountPasswordEncryption = null;
		try {
			UtilityZuBank.connectionManager();

			PreparedStatement statementFetchMaxAdminId = UtilityZuBank.con
					.prepareStatement(ConstantValue.FETCH_MAX_ADMIN_ID);

			ResultSet resultsetMaxAdminId = statementFetchMaxAdminId.executeQuery();

			while (resultsetMaxAdminId.next()) {
				maxAdminId = resultsetMaxAdminId.getInt("maxstudvalue");

			}
			accountNumber = maxAdminId + 1;

			accountPassword = UtilityZuBank.generateAccountPassword();

			SendMailDTO mailDto = new SendMailDTO();
			mailDto.setAccountId(String.valueOf(accountNumber));
			mailDto.setGmail(adminDto.getGmail());
			mailDto.setCustFname(adminDto.getAdmin_fname());
			mailDto.setPassword(accountPassword);

			CreateAccountCustomer.mailSendingList.add(mailDto);

			accountPasswordEncryption = UtilityZuBank.encryption(accountPassword);

			PreparedStatement statementInsertAdminAccountDetails = UtilityZuBank.con
					.prepareStatement(ConstantValue.INSERT_ADMIN_ACCOUNT_DETAILS);
			statementInsertAdminAccountDetails.setString(1, adminDto.getAdmin_fname());
			statementInsertAdminAccountDetails.setString(2, adminDto.getAdmin_lname());
			statementInsertAdminAccountDetails.setString(3, adminDto.getAadhar_id());
			statementInsertAdminAccountDetails.setString(4, adminDto.getGmail());
			statementInsertAdminAccountDetails.setString(5, adminDto.getPhone_no());
			statementInsertAdminAccountDetails.setString(6, accountPasswordEncryption);

			statementInsertAdminAccountDetails.executeUpdate();

			uploadFile(adminDto, accountNumber, file);

			UtilityZuBank.con.close();
		} catch (Exception e) {

		}
	}

	public void uploadFile(CreateAccountAdminDto adminDto, int accountNumber, Part file) {
		InputStream iosValue = null;

		try {
			iosValue = file.getInputStream();

			PreparedStatement statementInsertImageFilePath = UtilityZuBank.con
					.prepareStatement(ConstantValue.INSERT_ADMIN_IMAGE_FILE_PATH);
			statementInsertImageFilePath.setInt(1, accountNumber);
			statementInsertImageFilePath.setBlob(2, iosValue);
			statementInsertImageFilePath.executeUpdate();

		} catch (Exception e) {

		}

	}

	public JSONObject validationCheck(JSONObject inputValidationStatus) {

		JSONObject json = new JSONObject();
		Iterator<String> keys1 = inputValidationStatus.keys();

		while (keys1.hasNext()) {
			String key1 = (String) keys1.next();
			Object value1 = inputValidationStatus.get(key1);
			System.out.println("Key: " + key1);
			System.out.println("Value: " + value1);

			if (value1.equals("failure")) {

				json.put(key1, value1);

			}

		}
		return json;

	}

}
