package CustomerActions;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ZubankUtility.UtilityZuBank;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;




public class TransactionAsSender extends HttpServlet{
	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		int balance=0;
		
		String username=String.valueOf(req.getSession().getAttribute("user"));
		System.out.println(username);
		
	
        try {
        	
	           
	            JSONArray list2=sendertransfer(Integer.parseInt(username));


	            res.setContentType("application/json");
	      	     res.setCharacterEncoding("UTF-8");
	         	 res.getWriter().write(list2.toString());
	            
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

       
        
        
        
       
     
	}
	
	public JSONArray sendertransfer(int acnum) throws SQLException {
		int receiver_id;
        int amount=0;
        int s_balance=0;
        String date="";
        

        

        JSONArray list1 = new JSONArray();

        PreparedStatement stm = UtilityZuBank.con.prepareStatement("SELECT  * from transaction_details where sender_id = ?");
        stm.setInt(1, acnum);
        ResultSet rs = stm.executeQuery();

        rs.afterLast();
            while (rs.previous()) {

                receiver_id=rs.getInt("receiver_id");
                amount =rs.getInt("amount");
                s_balance = rs.getInt("sender_balance");
                date =rs.getString("date_time");


                 JSONObject json=new JSONObject();
                
                json.put("username",acnum);
	            json.put("receiver",receiver_id);
	            json.put("amount",amount);
	            json.put("sender_balance",s_balance);
	            json.put("date",date);
	            list1.add(json);  
            }
			return list1;
        
			
			
			
        }
	
	
	

  
	
}
