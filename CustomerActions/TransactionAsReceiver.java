package CustomerActions;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ZubankDto.TransactionAsReceiverDTO;
import ZubankUtility.UtilityZuBank;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class TransactionAsReceiver extends HttpServlet {

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		int balance=0;
		String sender_id = null;
		String amount = null;
		String receiver_balance = null;
		String date_time = null;
		
		String username=String.valueOf(req.getSession().getAttribute("user"));
		System.out.println(username);
		
	
        try {
        	UtilityZuBank.connectionManager();
			PreparedStatement stm = UtilityZuBank.con.prepareStatement("SELECT balance FROM bank_details where account_id = ?");
			 stm.setString(1, username);
			ResultSet rs = stm.executeQuery();

			List list = receivertransfer(Integer.parseInt(username));

			// ArrayList<JSONObject> list1=new ArrayList();
			JSONArray list1 = new JSONArray();
			TransactionAsReceiverDTO dto;

			for (int i = 0; i < list.size(); i++) {

				dto = (TransactionAsReceiverDTO) list.get(i);

				sender_id = dto.getSender();
				amount = dto.getAmount();
				receiver_balance = dto.getReceiver_balance();
				date_time = dto.getDate();

				JSONObject json = new JSONObject();
				System.out.println(sender_id);
				System.out.println(amount);
				System.out.println(receiver_balance);
				System.out.println(date_time);
				json.put("username", username);
				json.put("sender", sender_id);
				json.put("amount", amount);
				json.put("receiver_balance", receiver_balance);
				json.put("date", date_time);

				list1.add(json);
			}

			res.setContentType("application/json");
			res.setCharacterEncoding("UTF-8");
			res.getWriter().write(list1.toString());

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
     
	}
	
	  public List receivertransfer(int acnum) throws SQLException {
	        int sender_id=0;
	        int amount=0;
	        int r_balance=0;
	        int receiver=0;
	        int receiver_count=0;
	        int S_No=1;
	        int count=0;
	        String data="";
	        String date="";
	        ArrayList<Object> list1=new ArrayList();
	        
	        PreparedStatement stm = UtilityZuBank.con.prepareStatement("SELECT  * from transaction_details where receiver_id = ?");
	        stm.setInt(1, acnum);
	        ResultSet rs = stm.executeQuery();

	        

	        rs.afterLast();
	        while (rs.previous()) {

	        	TransactionAsReceiverDTO dto =new TransactionAsReceiverDTO();
	        	
	            sender_id=rs.getInt("sender_id");
	            amount =rs.getInt("amount");
	            r_balance = rs.getInt("receiver_balance");
	            date =rs.getString("date_time");

	            dto.setSender(String.valueOf(sender_id));
	            dto.setAmount(String.valueOf(amount));
	            dto.setReceiver_balance(String.valueOf(r_balance));
	            dto.setDate(date);
	            
	            list1.add(dto);
	            
	        }
			return list1;
	        

	    }
}
