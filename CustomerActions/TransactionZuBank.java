package CustomerActions;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ZubankDto.TransactionDto;
import ZubankUtility.UtilityZuBank;

public class TransactionZuBank extends HttpServlet{
    static Map<String, List<TransactionDto>> transactionmap = new HashMap<String, List<TransactionDto>>();

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		String sender_id=req.getParameter("sender_id");
		String reciever_id=req.getParameter("reciever_id");
		String amount=req.getParameter("amount");
		String account_password=req.getParameter("account_password");
		System.out.print("success.....");
		
		System.out.println(sender_id);
		System.out.println(reciever_id);
		System.out.println(amount);
		System.out.println(account_password);
		
		try {
			transfermoney(sender_id, reciever_id, amount, account_password);
			res.sendRedirect("CustomerViewPage.jsp?status=success");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	
	public  TransactionDto transfermoney(String sender,String reciever,String aum,String password) throws SQLException, ClassNotFoundException{
		
		try{
			UtilityZuBank.connectionManager();
			int balance;
			int sbalance;
		    int rbalance;
		    int amount=0;
		    // validate method for all three inputs, sender and receiver, are they really part of the system. if it returns jsonObject, proceed, if it returns jsonobject, throw new customException(reason);
		    // {"status": "success"}, {"status": "failure", "reason": "Error while parsing sender id"}
		    try{
		    	amount = Integer.parseInt(aum);
		    }catch(NumberFormatException nfe) {
		    	amount = 0;
		    }
		    int sender_id=Integer.parseInt(sender);
		    int reciever_id=Integer.parseInt(reciever);
		    int arr[]={sender_id,reciever_id};
		    int arrar[]=new int[2];

		    for (int i=0;i<arr.length;i++){
		    	PreparedStatement stm1 =  UtilityZuBank.con.prepareStatement("SELECT * from bank_details  where account_id=?");
		        stm1.setInt(1,arr[i]);
		        ResultSet rs1 = stm1.executeQuery();
		        while (rs1.next()) {

		                balance = rs1.getInt("balance");

		                   arrar[i]=balance;
		            }
		        }
		        System.out.println("Balance : "+ Arrays.toString(arrar));

		        sbalance=arrar[0]-amount;
		        rbalance=arrar[1]+amount;

		        System.out.println(sbalance);
		        System.out.println(rbalance);
		       
		        TransactionDto dto=new TransactionDto();
		        dto.setSender_id(sender);
		        dto.setReciever_id(reciever);
		        dto.setAmount(amount);
		        System.out.println("***********");
		        ArrayList <TransactionDto>  list = new ArrayList<TransactionDto>();
       
		        if(transactionmap.containsKey(dto.getSender_id())){
		        	//System.out.println("---------"+transactionmap.get(dto.getSender_id()).iterator().next().getSender_id());
		        	transactionmap.get(dto.getSender_id()).add(dto);
		        	System.out.println("new value set");
		        }else{
		        	list.add(dto);
		        	 transactionmap.put(dto.getSender_id(), list);
		        }
		    
		        for (Entry<String, List<TransactionDto>> entry : transactionmap.entrySet()){
		        	 System.out.println("Key = " + entry.getKey()); 
		           for(int i=0;i<transactionmap.get(entry.getKey()).size();i++){
		        	   System.out.println("Value = " + transactionmap.get(entry.getKey()).get(i).getAmount());
			           
		           }
		            
		            
		            

		        } 
		        System.out.println(transactionmap.size());
		       
//		        int arrary[]={sbalance,rbalance};
//
//		        for (int i=0;i<arr.length;i++){
//		            PreparedStatement stm =  UtilityZuBank.con.prepareStatement(" UPDATE bank_details set balance = ? where account_id =?");
//		            stm.setInt(1,arrary[i]);
//		            stm.setInt(2,arr[i]);
//		            int rs = stm.executeUpdate();
//
//		        } 
//		            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//		            Date date = new Date();
//		            String date_time=formatter.format(date);
//		            System.out.println(date_time);
//
//
//		            PreparedStatement stm2 = UtilityZuBank.con.prepareStatement("insert into transaction_details (sender_id,receiver_id,amount,sender_balance,receiver_balance,date_time)values(?,?,?,?,?,?)");
//		            stm2.setInt(1, sender_id);
//		            stm2.setInt(2, reciever_id);
//		            stm2.setInt(3, amount);
//		            stm2.setInt(4, sbalance);
//		            stm2.setInt(5, rbalance);
//		            stm2.setString(6, date_time);
//		            int k = stm2.executeUpdate();
//		            
//		            
		        
		        UtilityZuBank.con.close();
			
		}catch (Exception e) {
			System.out.println("Error occured while transaction. Please try again later.");
		}
		
		
		return null;
		
	}
}
