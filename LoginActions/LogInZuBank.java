package LogIn;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ZubankDto.LoginDTO;
import ZubankUtility.ConstantValue;



public class LogInZuBank extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	
protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		if(req.getSession().getAttribute("Role") == null){
			
			LoginDTO loginDto = new LoginDTO();
			
			loginDto.setUsername(req.getParameter(ConstantValue.USERNAME));
			loginDto.setPassword(req.getParameter("password"));
			System.out.print("success.");
			
			LoginDTO logg = LogInCheckZuBank.Check(loginDto);
			
			
			if(logg.isCorrect()){
				
				HttpSession session = req.getSession(false);
				
				if (session != null && !session.isNew()) {
				    session.invalidate();
				}
				
				session = req.getSession(true);
				session.setAttribute("user", logg.getMailid());
				session.setAttribute("Role","true");
				session.setMaxInactiveInterval(10000);


				res.addHeader("Set-Cookie", "csrf="+System.currentTimeMillis()+loginDto.getUsername()+";");	
				res.addHeader("Set-Cookie", " identification="+loginDto.getUsername()+";");

				
				
				if(session.getAttribute("Role").equals("true")){
					
					if(logg.getRole() == "CUSTOMER"){
						 res.sendRedirect("893fcf61e0dbffd3bf148e7c78aa11f377b831f61aa99254ec88ea763fe21493");	
					}else if(logg.getRole() == "ADMIN"){
						res.sendRedirect("AdminViewPage.jsp");	
					}
	           
				}
		
	}
			else{
				
				res.sendRedirect("LogInPageZuBank.jsp?status=false");
				
			}

}
}
}
