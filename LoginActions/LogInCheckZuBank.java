package LogIn;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ZubankDto.LoginDTO;
import ZubankUtility.ConstantValue;
import ZubankUtility.UtilityZuBank;

public class LogInCheckZuBank {
	
	
	public static LoginDTO Check(LoginDTO loginDto) {
		LoginDTO logg = new LoginDTO();
		int acnum=Integer.parseInt(loginDto.getUsername())/10000000;
		
		System.out.println(acnum);
		String selectquery ="";
		
		String encryptedpassword =UtilityZuBank.encryption(loginDto.getPassword());

		if (acnum==1) {
			selectquery= ConstantValue.LOGIN_SELECT_QUERY_CUSTOMER;
		}else if(acnum==9){
			selectquery= ConstantValue.LOGIN_SELECT_QUERY_ADMIN;
		}
			
		try{
			
			UtilityZuBank.connectionManager();
			
			System.out.println("first---"+loginDto.getUsername());
			System.out.println("first---"+encryptedpassword);
			System.out.println();

            PreparedStatement statement = UtilityZuBank.con.prepareStatement(selectquery);
            statement.setInt(1, Integer.parseInt(loginDto.getUsername()));
            statement.setString(2,encryptedpassword);
            ResultSet rs = statement.executeQuery();
            
            if(acnum==1){
            	logg.setRole("CUSTOMER");
            }else if(acnum==9){
            	logg.setRole("ADMIN");
            }
            System.out.println("second---"+loginDto.getUsername());
			System.out.println("second---"+encryptedpassword);
			System.out.println();
           boolean bool=rs.next();
            
           System.out.println("bool ="+bool);
            if(bool){
                
            	logg.setMailid(Integer.parseInt(loginDto.getUsername()));
            	logg.setCorrect(bool);
            }
		
    }catch(Exception e){ System.out.println("\nenter the correct username and prd.\n");}
	finally{
		try{
			UtilityZuBank.con.close();
		}catch(SQLException ioe) {
			
		}
			if(logg.getMailid()==0){
				logg.setCorrect(false);
			}
		}
    
    return logg;
    }
	
	
	

}
