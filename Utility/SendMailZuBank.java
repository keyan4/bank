package ZubankUtility;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import ZubankDto.SendMailDTO;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import javax.mail.Session;

public class SendMailZuBank {
	
	
	 public static void sendingMailForCustomer(SendMailDTO mailDto){
	        final String username = "customizedlearningforzoho@gmail.com";
	        final String password = "";
	        String greetings="";

	        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH");
	        LocalDateTime now = LocalDateTime.now();
	        String timeinString= dtf.format(now);
	        int time=Integer.parseInt(timeinString);

	        if (time>=8 && time<12){
	            greetings="Good Morning";
	        }else if(time>=12 && time<16){
	            greetings="Good AfterNoon";
	        }else if (time>=16){
	            greetings="Good Evening";
	        }

	        Properties props = new Properties();

	        props.put("mail.smtp.starttls.enable", "true");

	        props.put("mail.smtp.auth", "true");

	        props.put("mail.smtp.host", "smtp.gmail.com");

	        props.put("mail.smtp.port", "587");

	        Session session = Session.getInstance(props,
	                new javax.mail.Authenticator() {

	                    //override the getPasswordAuthentication method
	                    protected PasswordAuthentication getPasswordAuthentication() {
	                        return new PasswordAuthentication(username, password);
	                    }
	                });

	        //compose the message
	        try {


	            Message message = new MimeMessage(session);

	            message.setFrom(new InternetAddress("customizedlearningforzoho@gmail.com"));
	            message.setRecipients(Message.RecipientType.TO,
	                    InternetAddress.parse(mailDto.getGmail()));
	            message.setSubject("OTP for your Password");
	            message.setText(greetings+" "+mailDto.getCustFname()+",\n\n For your Account Number "+mailDto.getAccountId()+"\n your password number is \uD83D\uDCEC : "+mailDto.getPassword()+"\n For you atm card "+mailDto.getCardId()+"\n your pin number is \uD83D\uDCEC : "+mailDto.getPinNumber());
	            try {
	                Transport.send(message);
	            }catch(Exception ex){
	                System.out.println("please enter the correct G-mail id....");
	            }

	  

	        } catch (MessagingException e) {
	            throw new RuntimeException(e);
	        }
	    }
	 
	 public static boolean sendingmailforAdmin(SendMailDTO mailDto){
	        final String username = "customizedlearningforzoho@gmail.com";
	        final String password = "!@QW12qw";
	        String greetings="";

	        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH");
	        LocalDateTime now = LocalDateTime.now();
	        String timeinString= dtf.format(now);
	        int time=Integer.parseInt(timeinString);

	        if (time>=8 && time<12){
	            greetings="Good Morning";
	        }else if(time>=12 && time<16){
	            greetings="Good AfterNoon";
	        }else if (time>=16){
	            greetings="Good Evening";
	        }

	        Properties props = new Properties();

	        props.put("mail.smtp.starttls.enable", "true");

	        props.put("mail.smtp.auth", "true");

	        props.put("mail.smtp.host", "smtp.gmail.com");

	        props.put("mail.smtp.port", "587");

	        Session session = Session.getInstance(props,
	                new javax.mail.Authenticator() {

	                    //override the getPasswordAuthentication method
	                    protected PasswordAuthentication getPasswordAuthentication() {
	                        return new PasswordAuthentication(username, password);
	                    }
	                });

	        //compose the message
	        try {


	            Message message = new MimeMessage(session);

	            message.setFrom(new InternetAddress("customizedlearningforzoho@gmail.com"));
	            message.setRecipients(Message.RecipientType.TO,
	                    InternetAddress.parse(mailDto.getGmail()));
	            message.setSubject("OTP for your Password");
	            message.setText(greetings+" "+mailDto.getCustFname()+",\n\n For your Account Number "+mailDto.getAccountId()+"\n your password number is \uD83D\uDCEC : "+mailDto.getPassword());
	            try {
	                Transport.send(message);
	            }catch(Exception ex){
	                System.out.println("please enter the correct G-mail id....");
	            }

	            System.out.println("your otp is send to your \uD83D\uDCEC "+mailDto.getGmail());

	            return true;
	        } catch (MessagingException e) {
	            throw new RuntimeException(e);
	        }
	    }


}
